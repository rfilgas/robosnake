import {Agent, AgentA, AgentB, AgentC, AgentD, Player } from "./Agent";
import { Cell, GameScreen, draw } from "./GameScreen";
import { Point, SnakeState } from "./Snake";
import { scheduleNextUpdate, updateApples, updateLost } from "./DrawingLibrary";

export const CELLSIZE: number = 5;

// a MaybeCell is either a Cell or the string "outside"
export type MaybeCell = Cell | "outside";

// a ScreenPart is a CELLSIZE x CELLSIZE array of MaybeCell arrays
export type ScreenPart = MaybeCell[][];

/**
 * To Add a new agent:
 * 1. Create a new agent in agent.ts implementing the agent interface. This includes creating a function that
 *    recieves a screenpart as input and motion as output. Note that agents can control their own state and potential next moves independently.
 * 2. Add the agent type to the list of agents in the run function by modifying the list. They will match with the corresponding player by
 *    index value and are independent instances - the same agent type can be used more than once.
 *
 * To simply change the agent for a given player: Change the agent type in the corresponding index of the agents array in the run function.
 */
export function run(waitTimeInMilliseconds: number, newApplesEachStep: number, screen: GameScreen): void {
  const playerNames: Player[] = ["A", "B", "C", "D"];
  const agents: Agent[] = [<Agent> new AgentA, <Agent> new AgentB, <Agent> new AgentC, <Agent> new AgentD];
  const startPositions: number[][] = [[0,0],[screen.length-1, 0], [0, screen.length-1], [screen.length-1, screen.length-1]];
  const snakes: SnakeState[] = new Array<SnakeState>(startPositions.length);

  initiateSnakeStates(startPositions, agents, playerNames, snakes);
  setPlayerOnGrid(screen, snakes)
  draw(screen);
  scheduleNextUpdate(waitTimeInMilliseconds, () => step(waitTimeInMilliseconds, newApplesEachStep, screen, snakes));
  // the "() =>" part is important!
  // without it, step will get called immediately instead of waiting
}

export function plotMove(snake: SnakeState, moveLocation: Point, screen: GameScreen): void{
  snake.setPoint(moveLocation);
  screen[moveLocation.y][moveLocation.x] = snake.playerName;
}

function takeTurn(snake: SnakeState, screen: GameScreen): void {
  if (!snake.lost) {
    const moveLocation = snake.getLocation(screen)
    if (isEdgeOfScreen(moveLocation, screen))
      snake.lost = true;
    else
      switch (screen[moveLocation.y][moveLocation.x]) {
        case "empty": {
          plotMove(snake, moveLocation, screen)
          break;
        }
        case "apple": {
          plotMove(snake, moveLocation, screen)
          snake.apples++;
          break;
        }
        default: {
          snake.lost = true;
          break;
        }
      }
  }
}

function setPlayerOnGrid(screen: GameScreen, snakes: SnakeState[]): void {
  for (const snake of snakes) {
    screen[snake.y][snake.x] = snake.playerName;
  }
}

function initiateSnakeStates(startPositions: number[][], agents: Agent[], playerNames: Player[], snakes: SnakeState[]): void {
  for (let i = 0; i < snakes.length; i++) {
    snakes[i] = new SnakeState(startPositions[i][0], startPositions[i][1], agents[i], playerNames[i]);
  }
}

function isEdgeOfScreen(moveLocation: Point, screen: GameScreen): boolean {
  return moveLocation.x < 0 || moveLocation.y < 0 || moveLocation.x >= screen.length || moveLocation.y >= screen.length
}

function step(
  waitTimeInMilliseconds: number,
  newApplesEachStep: number,
  screen: GameScreen,
  snakes: SnakeState[]
): void {
  generateApples(newApplesEachStep, screen)
  rotatePlayersInOrder(snakes, screen);
  draw(screen);
  updateSnakeStats(snakes)
  if (gameIsNotOver(snakes))
    scheduleNextUpdate(waitTimeInMilliseconds, () => step(waitTimeInMilliseconds, newApplesEachStep, screen, snakes));
}

function generateApples(newApplesEachStep: number, screen: GameScreen): void {
  // generate new apples
  for (let i = 0; i < newApplesEachStep; i++) {
    // random integers in the closed range [0, screen.length]
    const x = Math.floor(Math.random() * screen.length);
    const y = Math.floor(Math.random() * screen.length);
    // if we generated coordinates that aren't empty, skip this apple
    if (screen[y][x] == "empty")
      screen[y][x] = "apple";
  }
}

function rotatePlayersInOrder(snakes: SnakeState[], screen: GameScreen): void {
    snakes.forEach(snake => {
      takeTurn(snake, screen)
  });

}

function updateSnakeStats(snakes: SnakeState[]): void {
  for (const snake of snakes) {
    updateLost(snake.playerName, snake.lost); updateApples(snake.playerName, snake.apples);
  }
}

function gameIsNotOver(snakes: SnakeState[]): boolean {
  for (const snake of snakes) {
    if (!snake.lost){
      return true
    }
  }
  return false
}