import {MaybeCell, ScreenPart } from "./GameRunner";

export type Motion = "up" | "down" | "left" | "right";
export type Player = "A" | "B" | "C" | "D";

export interface Agent{
    move(screenPart: ScreenPart): Motion;
}

/**
 * Agent A currently always turns right.
 */
export class AgentA implements Agent {
    public move(screenPart: ScreenPart): Motion {
        return "right";
    }
}

/**
 * Agent B currently chooses a random motion.
 */
export class AgentB implements Agent {
    public move(screenPart: ScreenPart): Motion {
        return randomMotion(screenPart);
    }
}

/**
 * Agent C currently cycles through moves.
 */
export class AgentC implements Agent {
    private cCycle: Motion[] = ["up", "up", "right", "down", "right"];
    private cIndex: number;

    constructor() {
        this.cIndex = 0
    }
    public move(screenPart: ScreenPart): Motion {
        const c: Motion = this.cCycle[this.cIndex];
        this.cIndex++;
        this.cIndex = this.cIndex % this.cCycle.length;
        return c;
    }
}

/**
 * Agent D goes for any nearby apple, otherwise chooses random.
 */
export class AgentD implements Agent {
    public move(screenPart: ScreenPart): Motion {
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                if (i > 3) return "right";
                else if (i < 3) return "left";
                else if (j > 3) return "down";
                else if (j < 3) return "up";
            }
        }
        return randomMotion(screenPart);
    }
}

function randomMotion(part: ScreenPart): Motion {
    const rnd: number = Math.random() * 4; // random float in the half-open range [0, 4)

    let x: Motion;
    if (rnd < 1) x = "up";
    else if (rnd < 2) x = "down";
    else if (rnd < 3) x = "left";
    else x = "right";

    // try not to hit anything
    if (tryMove(x, part) != "apple" && tryMove(x, part) != "empty") {
        switch (x) {
            case "up": return "down";
            case "right": return "left";
            case "down": return "up";
            case "left": return "right";
        }
    }
    return x;
    }

function tryMove(m: Motion, p: ScreenPart): MaybeCell {
    // the snake is positioned in the center at p[2][2]
    switch (m) {
        case "left": return p[2][1];
        case "right": return p[2][3];
        case "up": return p[1][2];
        case "down": return p[3][2];
        default: return p[2][2];
    }
}
