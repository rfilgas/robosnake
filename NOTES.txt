First, skim the code to get a feel for which parts are easy to read and which parts are hard to read.
Take a note of every part of the code that is hard for you to read, so that you know which parts will require
the most time and focus to understand.


Proposed class structure:

Agent type{
    var move
}

snake(agent_type, has_lost, window){
    var agent type
    var window
    var has_lost
    snake.toggle_lost()
    snake.change_window()
    snake.refresh_window(window_vals from environment)
    snake.take_turn()
}

environment{
    2d-array: env_map
    environment.update_square()
    environment.clear()
    environment.generateApples()
    environment.updateapples()
}

snakes(snake1: obj, snake2:obj, snake3:obj){
    dictionary snakes{
        snake_name, snake
    }
    set_states{
        snake1: {
            agent: agent_type
            window: window_info
            hasLost: False
        }
    }
}


How the code is called. Notes on changes to make in all caps.

MAIN{

    # REFACTOR CONSTANTS TO BE VARIABLES IN MAIN:
    updateLost...(letter(A|B|C|D), bool(TRUE/FALSE)) --> {
        set html element lostplayer# to bool input
    }
    updateApples...(letter(A|B|C|D), 0(any number)) --> {
        set html element applesplayer# to num apples
    }
    CHANGE SIZE TO SCREENSIZE
    screen = initialize -->{
        set screen size
        # MORE ELEGANT FOR LOOP USING VARIABLES
        initialize screen
    }
    run(const 1000, 10, screen){ --> GAMERUNNER.TS
        # USE SELF DOCUMENTING VARIABLE NAMES
        # REPLACE CONSTANTS WITH VARIABLES
        # MAKE INTO A FUNCTION
        initialize agent{
            # SET STATES FOR EVERY PLAYER - USE A CLASS
        }

        # MAKE A FUNCTION FOR THIS
        # KEEP FUNCTION SELF-DOCUMENTING
        draw player positions on screen

        schedulenextupdate{ steptime, lambda function --> step{ steptime, apples, screen, a,b,c,d} --> gamerunner.TS
            clear timeoutID if exists
            timeoutid = setTimeout (timeHandler, step(steptime, apples, screen, a,b,c,d snakestates){

                MAKE FUNCTION: GENERATE APPLES{
                    MAKE FUNCTION: GENERATE RANDOM INTEGERS 0-SCREENLENGTH
                    MAKE FUNCTION: ADDAPPLEIFVEXISTS
                }

                MAKE FUNCTION TAKE_TURNS{
                    RUN FOLLOWING FUNCTION FOR ALL INPUTS A,B,C,D{
                        NEXTLOCATION = locationaftermotion(agentmove, snake){
                            agentmove(agent, screenpart){
                                getscreenpart(screen, snake){
                                    CHANGE 5 TO A CONSTANT CALLED UNIT SIZE
                                    create a unitsize by unitsize array (maybe lets try to define this a different way){
                                        CREATE A FUNCTION IS_IN_BOUNDS
                                        CREATE XVAL AND YVAL VARIABLES
                                        if in bounds:
                                            THIS PART IS X VAL Y VAL
                                        ELSE:
                                            THIS PART IS OUTSIDE
                                    }

                                    MAKE FUNCTION: EXECUTE PLAYER MOVE BY CALLING PLAYER.MOVE{
                                        A{
                                            right
                                        }
                                        B{
                                            random{
                                                replace switch statement with a random selector, this is dumb
                                                #MAKE FUNCTION IF LEGALMOVE
                                                trymove{
                                                    return move val
                                                }
                                                if not apple and not empty it's outside or snake. Go the opposite direction
                                            }
                                        C{
                                            cycle through moves - make it a function
                                        }
                                        d{
                                            go for nearby apple or random{}
                                        }

                                        }
                                    }



                                }
                            }
                        }
                        MAKE SINGLE FUNCTION FOR PLAYER TO TAKE TURN{
                            IF EMPTY{
                                MAKE FUNCTION MAKE MOVE(next location)
                            }
                            IF APPLE{
                                MAKE FUNCTION EAT APPLE
                                CALL MAKE MOVE{

                                }
                            }
                            DEFAULT{
                                this is fine.
                                break;
                            }
                        }
                    }

                    return new point mapping
                }

                RENAME DRAW TO UPDATE SCREEN
                draw{
                    for each pixel (is there a map function for this? why 2 for loops???){
                        MAKE FUNCTION SET PIXEL{
                            # USE A DICTIONARY OR SOMETHING AND CALL FILL CELL ONCE
                            # RENAME LEFT AND TOP TO HORIZONTAAL_VAL AND VERTICAL_VAL
                            case: caseval{
                                fillCell(color,left, top){
                                    fill style
                                    fill rectangle{
                                        clear
                                        fill
                                        stroke
                                    }
                                }
                            }
                        }
                    }
                }

                #CREATE FUNCTION UPDATE STATS that updates all lost and all apples with a for loop
                updatelost(player, lost){
                    set lost element to boolean
                }
                updateApples{
                    set apples element to number
                }

                MAKE FUNCTION GAME_OVER AND RETURN TRUE OR FALSE. RUN IN WHILE LOOP, NOT RECURSION
                if (not everyone lost){
                    recursive call with new info
                }
            })
        }
    }

}
