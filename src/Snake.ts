import {Agent, Motion, Player } from "./Agent";
import { CELLSIZE, MaybeCell, ScreenPart} from "./GameRunner";
import { GameScreen} from "./GameScreen";

/**
 * The point class simply holds x and y coordinates for plotting points in the game.
 */
export class Point {
public x: number;
public y: number;

constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
}
}

/**
 * The SnakeState class contains information about apples gathered, win/loss state, the agent function, and the player name.
 */
export class SnakeState extends Point {
public apples: number;
public lost: boolean;
public agent: Agent;
public playerName: Player;

constructor(x: number, y: number, agent: Agent, playerName: Player) {
    super(x, y); // call Point constructor to set x and y
    this.apples = 0;
    this.lost = false;
    this.agent = agent;
    this.playerName = playerName;
}

public setPoint(p: Point): void {
    this.x = p.x;
    this.y = p.y;
}

public getLocation(screen: GameScreen): Point {
    const motion: Motion = this.getMove(screen);
    return this.locationAfterMotion(motion)
}

public getMove(screen: GameScreen): Motion {
    const screenPart = this.getScreenPart(screen);
    return this.agent.move(screenPart)
}

private locationAfterMotion(motion: Motion): Point {
    switch (motion) {
    case "left": return new Point(this.x-1, this.y);
    case "right": return new Point(this.x+1, this.y);
    case "up": return new Point(this.x, this.y-1);
    case "down": return new Point(this.x, this.y+1);
    default: return new Point(this.x, this.y);
    }
}
// x and y are the left and top coordinate of a 5x5 square region.
// cells outside the bounds of the board are represented with "outside"
private getScreenPart(screen: GameScreen): ScreenPart {
    const part = new Array<MaybeCell[]>(CELLSIZE);
    for (let j = 0; j < CELLSIZE; j++) {
    part[j] = new Array<MaybeCell>(CELLSIZE);
    for (let i = 0; i < CELLSIZE; i++) {
        if (this.outOfBounds(screen,i,j))
        part[j][i] = screen[this.y+j-2][this.x+i-2];
        else
        part[j][i] = "outside";
    }
    }
    return part
}

private outOfBounds(screen: GameScreen, i: number, j: number): boolean {
    return this.x+i-2 >= 0 && this.y-2 + j >= 0 && this.x-2 + i < screen.length && this.y-2 + j < screen.length
}
}

